# Flectra Community / ddmrp-1

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[ddmrp](ddmrp/) | 1.0.1.0.0| Demand Driven Material Requirements Planning
[ddmrp_adjustment](ddmrp_adjustment/) | 1.0.1.0.0| Allow to apply factor adjustments to buffers.
[procurement_service](procurement_service/) | 1.0.1.0.0| Allows to generate procurements from confirmed sale orders
[ddmrp_history](ddmrp_history/) | 1.0.1.0.0| Allow to store historical data of DDMRP buffers.
[ddmrp_exclude_moves_adu_calc](ddmrp_exclude_moves_adu_calc/) | 1.0.1.0.0| Define additional rules to exclude certain moves from ADU calculation
[ddmrp_mrp_production_request](ddmrp_mrp_production_request/) | 1.0.1.0.0| Allows to prioritize Manufacturing Requests in a DDMRP strategy.
[ddmrp_product_replace](ddmrp_product_replace/) | 1.0.1.0.0| Provides a assisting tool for product replacement.


